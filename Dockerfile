FROM adoptopenjdk/openjdk11:jdk-11.0.11_9-alpine
MAINTAINER Ted (loungouviny15@gmail.com)
VOLUME /tmp
EXPOSE 8888
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} employeemanager-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Dspring.profiles.active=dev","-jar","employeemanager-0.0.1-SNAPSHOT.jar"]

